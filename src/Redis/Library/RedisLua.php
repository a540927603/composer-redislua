<?php
namespace Redis\Library;

class RedisLua
{
    protected $redis;
	public function __construct()
    {
        $redis= new \Redis();
        $redis->connect($_ENV['REDIS_HOST'] ?? '127.0.0.1', $_ENV['REDIS_PORT'] ?? '6379');
        $redis->auth($_ENV['REDIS_PASSWORD'] ?? ''); 
        $redis->select($_ENV['REDIS_SELECT'] ?? 1);
        $this->redis = $redis;
    }
	/**
	 * @Author   yetaihao
	 * @DateTime 2021-03-09
	 * @return   [type]     [description]
	 */
    public function connectionRedis(){
        $aa = $this->redis->setex('test1', 100, 'test1');
        return ['a'=>$this->redis,'b'=>$_ENV['REDIS_HOST'],'aa'=>$aa];
    }
}